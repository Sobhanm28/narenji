import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:narenji/ui/screen/home_screen.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Narenji',
      theme: ThemeData(
        primarySwatch: Colors.orange,
        accentColor: Colors.white,
        fontFamily: "montserrat",
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: HomeScreen(),
    );
  }
}
