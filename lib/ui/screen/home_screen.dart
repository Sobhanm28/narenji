import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class HomeScreen extends StatelessWidget {
  width(BuildContext context, double d) {
    return MediaQuery.of(context).size.width * d;
  }

  height(BuildContext context, double d) {
    return MediaQuery.of(context).size.height * d;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        width: width(context, 1),
        height: height(context, 1),
        color: Theme.of(context).primaryColor,
        child: Column(
          children: [
            Expanded(
              flex: 7,
              child: Container(
                alignment: Alignment.bottomCenter,
                child: SingleChildScrollView(
                  scrollDirection: Axis.horizontal,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        "Narenji",
                        style: TextStyle(
                            fontFamily: "josefin",
                            fontSize: 50,
                            color: Theme.of(context).accentColor),
                      ),
                      SizedBox(
                        width: 10,
                      ),
                      Image.asset(
                        "assets/images/orange_slice.png",
                        width: 50,
                        height: 50,
                      )
                    ],
                  ),
                ),
              ),
            ),
            Spacer(
              flex: 2,
            ),
            Expanded(
              flex: 9,
              child: Container(
                alignment: Alignment.topCenter,
                child: Column(
                  children: [
                    Container(
                        alignment: Alignment.center,
                        width: width(context, 0.8),
                        height: 80,
                        color: Colors.orange.shade300,
                        child: TextField(
                          textAlign: TextAlign.center,
                          cursorColor: Theme.of(context).primaryColor,
                          style: TextStyle(color: Colors.white, fontSize: 18),
                          decoration: InputDecoration(
                            hintText: "Enter your URL",
                            hintStyle:
                                TextStyle(color: Colors.white, fontSize: 18),
                            border: InputBorder.none,
                          ),
                        )),
                    SizedBox(
                      height: 1,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Container(
                          width: width(context, 0.2),
                          height: 80,
                          color: Colors.orange.shade300,
                          child: Icon(
                            Icons.copy,
                            size: 35,
                            color: Colors.white,
                          ),
                        ),
                        SizedBox(
                          width: 1,
                        ),
                        Container(
                          alignment: Alignment.center,
                          width: width(context, 0.4),
                          height: 80,
                          color: Colors.orange.shade300,
                          child: Text(
                            "Shorten",
                            style: TextStyle(color: Colors.white, fontSize: 18),
                          ),
                        ),
                        SizedBox(
                          width: 1,
                        ),
                        Container(
                          width: width(context, 0.2),
                          height: 80,
                          color: Colors.orange.shade300,
                          child: Icon(
                            Icons.insert_chart_outlined_outlined,
                            size: 35,
                            color: Colors.white,
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
